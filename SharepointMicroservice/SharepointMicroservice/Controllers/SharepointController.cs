﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web.Http;
using SharepointMicroservice.Models;
using WebGrease.Activities;
using System.IO;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Principal;
using System.Text;
using Microsoft.Win32.SafeHandles;
using Newtonsoft.Json;
using NLog;

namespace SharepointMicroservice.Controllers
{
    [RoutePrefix("api/sharepoint")]
    public class SharepointController : ApiController
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private static readonly string OnCall = ConfigurationManager.AppSettings["OnCall"];
        private static readonly string _user = ConfigurationManager.AppSettings["User"];
        private static readonly string Password = ConfigurationManager.AppSettings["Password"];

        private static readonly string OnCallSchedule = ConfigurationManager.AppSettings["OnCallSchedule"];

        private static readonly string CriticalIncidents = ConfigurationManager.AppSettings["CriticalIncidents"];

        private static readonly string DowntimePlans = ConfigurationManager.AppSettings["DowntimePlans"];

        private static readonly string AttestationUrl = ConfigurationManager.AppSettings["Attestation"];
        private static readonly string VaccineUrl = ConfigurationManager.AppSettings["VaccineUrl"];
        private static readonly string Url = ConfigurationManager.AppSettings["InformaticsSharepoint"];

        public static string url = ConfigurationManager.AppSettings["PrimecompSharepoint"];

        private static readonly string InformaticsOut = ConfigurationManager.AppSettings["InformaticsOut"];
        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool LogonUser(string lpszUsername, string lpszDomain, string lpszPassword,
            int dwLogonType, int dwLogonProvider, out SafeTokenHandle phToken);
        private static readonly string UserName = ConfigurationManager.AppSettings["UserName"];
        private static readonly string Pass = ConfigurationManager.AppSettings["Pass"];
        private static readonly string Domain = ConfigurationManager.AppSettings["Domain"];
        // GET: api/Sharepoint
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        [Route("SendCovidAttestation")]
        [HttpPost]
        public IHttpActionResult SendCovidAttestation()
        {
            var headers = Request.Headers;
            var employee = "";
            var attestationDate = DateTime.Now;
            var resultType = "";
            var resultFlag = "";
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"SendCovidAttestation{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
            try
            {
                if (headers.Contains("employee"))
                {
                    Logger.Info("Employee Header Found");
                    employee = headers.GetValues("employee").First();
                    var date = headers.GetValues("AttestationDate").First();
                    attestationDate = Convert.ToDateTime(date);
                    resultType = headers.GetValues("ResultType").First();
                    resultFlag = headers.GetValues("ResultFlag").First();
                }

                if (employee != null)
                {
                    try
                    {
                        using (var clientContext = new ClientContext(AttestationUrl))
                        {
                            clientContext.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");
                            //clientContext.Credentials = CredentialCache.DefaultCredentials;
                            var targetList = clientContext.Web.Lists.GetByTitle("Attestation Results");

                            var oListItemCreationInformation = new ListItemCreationInformation();
                            var oItem = targetList.AddItem(oListItemCreationInformation);
                            oItem["Title"] = employee;
                            oItem["AttestationDate"] = attestationDate;
                            oItem["ResultType"] = resultType;
                            oItem["ResultFlag"] = resultFlag;

                            oItem.Update();
                            Logger.Info("Writing to Sharepoint...");
                            clientContext.ExecuteQuery();
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Error("Error occurred while sending results to Sharepoint " + e.Message);
                        return BadRequest("Error occurred while sending results to Sharepoint " + e.Message);
                    }
                }
            }
            catch (Exception e)
            {
                // some exception
                Logger.Fatal("Unknown Exception occurred " + e.Message);
                return BadRequest("Unknown Exception occurred " + e.Message);
            }

            return Ok();
        }
        // This is for sending the Employee Covid Vaccine checkin/out to Sharepoint from spreadsheet
        [Route("SendCovidVaccineCheckInOut")]
        [HttpPost]
        public IHttpActionResult SendCovidVaccineCheckInOut()
        {
            var headers = Request.Headers;
            var vaccine = new Vaccine();
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"SendCovidVaccineCheckInOut{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
            var location = "";
            var listName = "";
            try
            {
                Logger.Info("You made it to the Microservice");
                if (headers.Contains("vaccine"))
                {
                    Logger.Info("vaccine header found");
                    
                    var vacFirst = headers.GetValues("vaccine").First();
                    Logger.Info("Vaccine Object is " + vacFirst);
                    vaccine = JsonConvert.DeserializeObject<Vaccine>(vacFirst);
                }

                if (headers.Contains("Location"))
                {
                    Logger.Info("header Contains Locaiton");
                    location = headers.GetValues("Location").FirstOrDefault();
                    listName = "COVID Vaccination Check In " + location;
                }
                if (vaccine != null)
                {
                    try
                    {
                        using (var clientContext = new ClientContext(VaccineUrl))
                        {
                            
                            clientContext.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");
                            var targetList = clientContext.Web.Lists.GetByTitle(listName);
                            var camlQuery= new CamlQuery();
                            var qs = String.Format(
                                "<View><Query><Where><Eq><FieldRef Name=\"txtDate\" /><Value Type=\"Text\">"+vaccine.ApptTimeString+"</Value></Eq></Where></Query></View>");
                            camlQuery.ViewXml=qs;
                            var existingItems = targetList.GetItems(camlQuery);
                            clientContext.Load(existingItems);    
                            clientContext.ExecuteQuery();
                           
                            try
                            {
                                if (Enumerable.Any(existingItems, item => item["Name"] != null && item["Name"].ToString() == vaccine.Name))
                                {
                                    Logger.Info("Record already exists so skipping: " + vaccine.Name + " : " + vaccine.ApptTime);
                                    return Ok();
                                }
                            }
                            catch (Exception e)
                            {
                                Logger.Error("Exception occurred checking for duplicate....");
                                return BadRequest(e.Message);
                            }
                            
                            var oListItemCreationInformation = new ListItemCreationInformation();
                            var oItem = targetList.AddItem(oListItemCreationInformation);
                            oItem["Title"] = vaccine.LastName;
                            oItem["First_x0020_Name"] = vaccine.FirstName;
                            oItem["EE_x0020_or_x0020_NPI"] = vaccine.EEorNPI;
                            oItem["Name"] = vaccine.Name;
                            oItem["Appt_x0020_Time"] = vaccine.ApptTime;
                            oItem["txtDate"] = vaccine.ApptTimeString;
                            oItem["Consent_x0020_Received"] = vaccine.ConsentReceived;
                            oItem["Baptist_x0020_EE"] = vaccine.BaptistEE;
                            oItem["Dept_x0020_Name"] = vaccine.DeptName;
                            oItem["Job_x0020_Title"] = vaccine.JobTitle;
                            oItem["Dose_x0020__x0028_1_x002f_2_x002"] = vaccine.Dose;
                            oItem["Arm_x0020__x0028_R_x002f_L_x0029"] = vaccine.Arm;
                            oItem["Reaction_x0020__x0028_Y_x002f_N_"] = vaccine.Reaction;
                            oItem["Notes"] = vaccine.Notes;
                            oItem["CommunityOrAffiliate"] = vaccine.CommunityOrAffiliate;
                            oItem["Location"] = vaccine.Location;
                            oItem.Update();
                            //Logger.Info("Saving to Sharepoint....");
                            clientContext.ExecuteQuery();
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Error("Error occurred while sending results to Sharepoint List " + listName, " " + e.Message);
                        return BadRequest("Error occurred while sending results to " + listName + " " + e.Message);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Fatal("Unknown Exception occurred :: " + listName + " :: Sharepoint", e.Message);
                // some exception
                return BadRequest("Unknown Exception occurred :: " + listName + " :: Sharepoint" + e.Message);
            }

            return Ok();
        }
        [Route("people")]
        [HttpGet]
        public IHttpActionResult GetEmail()
        {
            var headers = Request.Headers;
            var email = "";
            var title = "";
            var username = "";
            var password = "";
            try
            {
                var config = new NLog.Config.LoggingConfiguration();
                var logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"people{DateTime.Today:MM-dd-yy}.log" };
                config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
                NLog.LogManager.Configuration = config;
                if (headers.Contains("title"))
                {
                    title = headers.GetValues("title").First();
                    username = headers.GetValues("username").First();
                    password = headers.GetValues("password").First();
                }

                if (title != null)
                {
                    var ps = new People.People { UseDefaultCredentials = false };
                    try
                    {
                        ps.Credentials = new NetworkCredential(username, password, "bh");
                        var plist = ps.SearchPrincipals(title, 1, People.SPPrincipalType.User);

                        if (plist.Length > 0)
                        {
                            email = plist[0].Email;
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Error("Error occurred while calling People Web Reference " + e.Message);
                        return BadRequest("Error occurred while calling People Web Reference " + e.Message);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Fatal("Exception occurred " + e.Message);
                // some exception
                return BadRequest("Exception occurred " + e.Message);
            }

            return Ok(email);
        }

        [Route("GetDowntimeSchedule")]
        [HttpGet]
        public IHttpActionResult GetDowntimeSchedule()
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"GetDowntimeSchedule{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
            var schedules = new List<ScheduledQuarterlyDownTimePlansItem>();

            using (var ctx = new ClientContext(DowntimePlans))
            {
                ctx.Credentials = CredentialCache.DefaultCredentials;
                var list = ctx.Web.Lists.GetById(new Guid("cd562756-8074-44f1-ac52-4580ec4b029d"));
                var view = list.Views.GetByTitle("All Tasks");

                ctx.Load(view);

                var query = new CamlQuery();
                if (ctx.HasPendingRequest)
                {
                    ctx.ExecuteQuery();
                }

                query.ViewXml = $"<View><Query>{view.ViewQuery}{query}</Query></View>";

                var items = list.GetItems(query);

                ctx.Load(items);

                if (ctx.HasPendingRequest)
                {
                    ctx.ExecuteQuery();
                }

                foreach (var item in items)
                {
                    try
                    {
                        var plan = new ScheduledQuarterlyDownTimePlansItem { Title = item["Title"]?.ToString() };

                        if (item["AssignedTo"] != null)
                        {
                            var val = (FieldUserValue[])item["AssignedTo"];
                            var userList = val.Select(t => t.LookupValue).ToList();
                            plan.AssignedTo = userList;
                        }
                        plan.ChangeControlNumber = item["Change_x0020_Control_x0020_Numbe"]?.ToString();
                        plan.ChangeControlDescription = item["Body"]?.ToString();
                        plan.ChangeStartDate = (DateTime?)item["StartDate"];
                        plan.ChangeEndDate = (DateTime?)item["DueDate"];
                        plan.DownTimeCoordinatorComments = item["Comments"]?.ToString();
                        plan.ActualStartDate = (DateTime?)item["Actual_x0020_Start_x0020_Date"];
                        plan.ActualEndDate = (DateTime?)item["Actual_x0020_End_x0020_Date"];
                        schedules.Add(plan);
                    }
                    catch (Exception e)
                    {
                        Logger.Error("Exception occurred " + e.Message);
                    }
                }

                return Ok(schedules);
            }
        }
        [Route("GetDepartments")]
        [HttpGet]
        public IHttpActionResult GetDepartments()
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"GetDepartments{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
            var departments = new List<DepartmentsItem>();

            using (var ctx = new ClientContext(OnCallSchedule))
            {
                ctx.Credentials = new System.Net.NetworkCredential(_user, Password, "bh");
                var list = ctx.Web.Lists.GetByTitle("Departments");
                var view = list.Views.GetByTitle("All Items");

                ctx.Load(view);

                var query = new CamlQuery();
                if (ctx.HasPendingRequest)
                {
                    ctx.ExecuteQuery();
                }
                query.ViewXml = $"<View><Query>{view.ViewQuery}{query}</Query></View>";

                var items = list.GetItems(query);

                ctx.Load(items);

                if (ctx.HasPendingRequest)
                {
                    ctx.ExecuteQuery();
                }

                foreach (var item in items)
                {
                    try
                    {
                        var d = new DepartmentsItem
                        {
                            Attachments = (bool)item["Attachments"],
                            Created = (DateTime?)item["Created"]
                        };
                        //d.ContentType = (item["ContentType"] != null) ? item["ContentType"].ToString() : null;
                        if (item["Author"] != null)
                        {
                            var val = (FieldUserValue)item["Author"];
                            d.CreatedBy = val.LookupValue;
                        }
                        d.Department = item["Title"]?.ToString();
                        d.Id = (int)item["ID"];
                        d.Modified = (DateTime?)item["Modified"];
                        if (item["Editor"] != null)
                        {
                            var val = (FieldUserValue)item["Editor"];
                            d.ModifiedBy = val.LookupValue;
                        }
                        d.Version = item["_UIVersionString"]?.ToString();
                        d.Department = item["Title"]?.ToString();
                        departments.Add(d);
                    }
                    catch (Exception e)
                    {
                        Logger.Error("Exception occurred " + e.Message);
                    }
                }

                return Ok(departments);
            }
        }
        [Route("GetCriticalIncidents")]
        [HttpGet]
        public IHttpActionResult GetCriticalIncidents()
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"GetCriticalIncidents{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
            var incidents = new List<IncidentManagement>();
            var localZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            try
            {
                using (var ctx = new ClientContext(CriticalIncidents))
                {
                    ctx.Credentials = new System.Net.NetworkCredential(_user, Password, "bh");
                    var list = ctx.Web.Lists.GetByTitle("Incident Status Board");
                    var view = list.Views.GetByTitle("All Items");

                    ctx.Load(view);

                    var query = new CamlQuery();
                    if (ctx.HasPendingRequest)
                    {
                        ctx.ExecuteQuery();
                    }
                    query.ViewXml = $"<View><Query>{view.ViewQuery}{query}</Query></View>";

                    var items = list.GetItems(query);

                    ctx.Load(items);

                    if (ctx.HasPendingRequest)
                    {
                        ctx.ExecuteQuery();
                    }

                    foreach (var item in items)
                    {

                        var incident = new IncidentManagement();
                        if (item["AppAuthor"] != null)
                        {
                            var val = (FieldUserValue)item["AppAuthor"];
                            incident.AppCreatedBy = val.LookupValue;
                        }
                        if (item["AppEditor"] != null)
                        {
                            var val = (FieldUserValue)item["AppEditor"];
                            incident.AppModifiedBy = val.LookupValue;
                        }
                        incident.Attachments = (bool)item["Attachments"];
                        if (item["Clinical_x0020_Informatics"] != null)
                        {
                            var val = (FieldUserValue[])item["Clinical_x0020_Informatics"];
                            var userList = val.Select(t => t.LookupValue).ToList();
                            incident.ClinicalInformatics = userList;
                        }
                        incident.Created = (DateTime?)item["Created"];
                        if (item["Author"] != null)
                        {
                            var val = (FieldUserValue)item["Author"];
                            incident.CreatedBy = val.LookupValue;
                        }
                        incident.IncidentStart = (DateTime?)item["Date_x0020_and_x0020_Time_x0020_"];
                        incident.DowntimeProceduresInitiated = (bool)item["Downtime_x0020_Procedures_x0020_"];
                        if (incident.DowntimeProceduresInitiated)
                        {
                            var decisionForDowntime = (DateTime?)item["Decision_x0020_for_x0020_Downtim"] ?? DateTime.Now;
                            incident.DecisionForDowntime = TimeZoneInfo.ConvertTimeFromUtc(decisionForDowntime, localZone).ToString("MM/dd/yyyy hh:mm tt");
                        }
                        else
                        {
                            incident.DecisionForDowntime = null;
                        }
                        incident.EndUserImpact = item["End_x0020_User_x0020_Impact"]?.ToString();
                        incident.TimeToResolve = (DateTime?)item["Est_x002e__x0020_Time_x0020_to_x"];
                        incident.FolderChildCount = item["FolderChildCount"]?.ToString();
                        incident.ID = int.Parse(item["ID"].ToString());
                        incident.ImpactedServices = item["Systems_x0020_impacted"]?.ToString();
                        incident.IncidentCausedByChangeControl = item["Incident_x0020_Caused_x0020_by_x"]?.ToString();
                        if (item["Incident_x0020_Manager_x0020__x0"] != null)
                        {
                            var val = (FieldUserValue[])item["Incident_x0020_Manager_x0020__x0"];
                            var userList = val.Select(t => t.LookupValue).ToList();
                            incident.IncidentManager = userList;
                        }
                        incident.IncidentName = item["Title"]?.ToString();
                        incident.IncidentNumber = item["ESM_x0020_Parent_x0020_Ticket_x0"]?.ToString();
                        incident.ItemChildCount = item["ItemChildCount"]?.ToString();
                        var lastUpdate = (DateTime?)item["Last_x0020_Update"] ?? DateTime.Now;

                        incident.LastUpdate = TimeZoneInfo.ConvertTimeFromUtc(lastUpdate, localZone).ToString("MM/dd/yyyy hh:mm tt");
                        incident.LocationsAffected = (string[])item["Locations_x0020_Impacted"];

                        if (item["Editor"] != null)
                        {
                            var val = (FieldUserValue)item["Editor"];
                            incident.ModifiedBy = val.LookupValue;
                        }
                        var nextUpdate = (DateTime?)item["Next_x0020_Update"] ?? DateTime.UtcNow;
                        try
                        {
                            incident.NextUpdate = TimeZoneInfo.ConvertTimeFromUtc(nextUpdate, localZone).ToString("MM/dd/yyyy hh:mm tt");
                        }
                        catch (Exception)
                        {
                            incident.NextUpdate = nextUpdate.ToString("MM/dd/yyyy hh:mm tt");
                        }
                        incident.Number = item["Number"]?.ToString();
                        if (item["Service_x0020_Continuity_x0020_M"] != null)
                        {
                            var val = (FieldUserValue[])item["Service_x0020_Continuity_x0020_M"];
                            var managers = val.Select(t => t.LookupValue).ToList();
                            incident.ServiceContinuityManager = managers;
                        }
                        incident.StatusUpdates = item["Status_x0020_Updates"]?.ToString();
                        incident.Version = item["_UIVersionString"]?.ToString();
                        var modifiedDate = (DateTime?)item["Modified"] ?? DateTime.Now;
                        incident.Modified = TimeZoneInfo.ConvertTimeFromUtc(modifiedDate, localZone);

                        if (incident.Modified > DateTime.Now.AddHours(-72))
                        {
                            incidents.Add(incident);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred " + e.Message);
            }
            return Ok(incidents);
        }
        [Route("GetOnCallSchedule")]
        [HttpGet]
        public IHttpActionResult GetOnCallSchedule()
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"GetOnCallSchedule{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
            var schedules = new List<OnCallScheduleItem>();
            try
            {
                using (var ctx = new ClientContext(OnCallSchedule))
                {
                    ctx.Credentials = new System.Net.NetworkCredential(_user, Password, "bh");
                    var list = ctx.Web.Lists.GetByTitle("Schedule");
                    var view = list.Views.GetByTitle("All Items");

                    ctx.Load(view);

                    var query = new CamlQuery();
                    if (ctx.HasPendingRequest)
                    {
                        ctx.ExecuteQuery();
                    }
                    query.ViewXml = $"<View><Query>{view.ViewQuery}{query}</Query></View>";

                    var items = list.GetItems(query);

                    ctx.Load(items);

                    if (ctx.HasPendingRequest)
                    {
                        ctx.ExecuteQuery();
                    }

                    foreach (var item in items)
                    {
                        var i = new OnCallScheduleItem
                        {
                            Attachments = (bool)item["Attachments"],
                            Created = (DateTime?)item["Created"]
                        };
                        if (item["Author"] != null)
                        {
                            var val = (FieldUserValue)item["Author"];
                            i.CreatedBy = val.LookupValue;
                        }
                        i.Department = item["Department"]?.ToString();
                        i.E1Phone1 = item["e1Phone1"]?.ToString();
                        i.E1Phone1_Type = item["e1Phone1_Type"]?.ToString();
                        i.E1Phone2 = item["e1Phone2"]?.ToString();
                        i.E1Phone2_Type = item["e1Phone2_Type"]?.ToString();
                        i.E1Phone3 = item["e1Phone3"]?.ToString();
                        i.E1Phone3_Type = item["e1Phone3_Type"]?.ToString();
                        i.E2Phone1 = item["e2Phone1"]?.ToString();
                        i.E2Phone1_Type = item["e2Phone1_Type"]?.ToString();
                        i.E2Phone2 = item["e2Phone2"]?.ToString();
                        i.E2Phone2_Type = item["e2Phone2_Type"]?.ToString();
                        i.E2Phone3 = item["e2Phone3"]?.ToString();
                        i.E2Phone3_Type = item["e2Phone3_Type"]?.ToString();
                        i.EndDateTime = (DateTime?)item["End_x0020_Date_x0020_Time"];
                        i.Escalation1 = item["Escalation1"]?.ToString();
                        i.Escalation2 = item["Escalation2"]?.ToString();
                        i.Id = (int)item["ID"];
                        i.Modified = (DateTime?)item["Modified"];
                        if (item["Editor"] != null)
                        {
                            var val = (FieldUserValue)item["Editor"];
                            i.ModifiedBy = val.LookupValue;
                        }
                        i.PPhone1 = item["pPhone1"]?.ToString();
                        i.PPhone1_Type = item["pPhone1_Type"]?.ToString();
                        i.PPhone2 = item["pPhone2"]?.ToString();
                        i.PPhone2_Type = item["pPhone2_Type"]?.ToString();
                        i.PPhone3 = item["pPhone3"]?.ToString();
                        i.PPhone3_Type = item["pPhone3_Type"]?.ToString();
                        i.Primary = item["Primary"]?.ToString();
                        i.StartDateTime = (DateTime?)item["Start_x0020_Date_x0020_Time"];
                        i.Title = item["Title"]?.ToString();
                        i.Version = item["_UIVersionString"]?.ToString();

                        schedules.Add(i);
                    }

                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred " + e.Message);
            }
            return Ok(schedules);
        }
        [Route("GetOnCallPerson")]
        [HttpGet]
        public IHttpActionResult GetOnCallPerson()
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"GetOnCallPerson{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
            try
            {
                var person = new Person();
                var title = "";
                var phone1 = "";
                var schedule = GetSchedule();
                ListItem item = null;
                var listItems = schedule as ListItem[] ?? schedule.ToArray();
                try
                {
                    item = listItems.First(x => (x.FieldValues["Start_x0020_Date_x0020_Time"] != null &&
                                                 (DateTime)x.FieldValues["Start_x0020_Date_x0020_Time"] <=
                                                 DateTime.Now) &&
                                                (x.FieldValues["End_x0020_Date_x0020_Time"] != null &&
                                                 (DateTime)x.FieldValues["End_x0020_Date_x0020_Time"] >=
                                                 DateTime.Now));
                }
                catch (Exception)
                {
                    item = listItems.OrderByDescending(x => x.FieldValues["Start_x0020_Date_x0020_Time"]).First();
                }
                if (item == null)
                {
                    item = listItems.OrderByDescending(x => x.FieldValues["Start_x0020_Date_x0020_Time"]).First();
                }
                if (item?["Primary"] != null)
                {
                    var val = (FieldLookupValue)item["Primary"];
                    title = val.LookupValue;
                }

                // get information about person.
                var persons = GetPersons();

                item = persons.FirstOrDefault(x =>
                    x.FieldValues["Title"] != null && x.FieldValues["Title"].ToString() == title);

                if (item != null)
                {
                    phone1 = item["Phone_x0020_1"].ToString();
                }

                person.Title = title;
                person.Phone1 = phone1;
                return Ok(person);
            }
            catch (Exception e)
            {
                Logger.Error("An Exception occurred getting On Call Person " + e.Message);
                return BadRequest("An Exception occurred getting On Call Person " + e.Message);
            }
        }
        [Route("GetAncillaryProviders")]
        [HttpGet]
        public IHttpActionResult GetAncillaryProviders()
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"GetAncillaryProviders{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
            try
            {
                List<Provider> provList = new List<Provider>();
                ClientContext clientContext = new ClientContext(url);
                clientContext.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");

                if (clientContext != null)
                {
                    var web = clientContext.Web;
                    List list = web.Lists.GetByTitle("Ancillary Providers");
                    var listItemCollection = list.GetItems(CamlQuery.CreateAllItemsQuery());

                    clientContext.Load(listItemCollection,
                        eachItem => eachItem.Include(
                            item => item,
                             item => item["Status"], //Status
                             item => item["ID"], //Access ID
                             item => item["Title"], // Group  Name ?? 
                                                    //item => item["Contract_x003a_Entity_x0020_Name"], //entity name ??
                             item => item["City"], //  City
                             item => item["Fax_x0020_Number"], //  Fax
                             item => item["Phone"], // Phone
                             item => item["State"], //  State
                             item => item["County"], // County
                             item => item["Zip"], //  Zip
                             item => item["Address"], //  Address
                             item => item["Specialty"])); // specialty

                    clientContext.ExecuteQuery();

                    foreach (var item in listItemCollection)
                    {
                        try
                        {
                            Provider p = new Provider();
                            if (item["Status"].Equals("Active"))
                            {
                                p.ID = item["ID"].ToString();
                                if (item["Title"] != null)
                                {
                                    //FieldLookupValue val = (FieldLookupValue)item["Title"];
                                    p.GroupName = item["Title"].ToString();// val.LookupValue;
                                }
                                LocationInformation li = new LocationInformation();
                                if (item["City"] != null)
                                {
                                    li.City = item["City"].ToString();
                                }
                                if (item["Fax_x0020_Number"] != null)
                                {
                                    li.Fax = item["Fax_x0020_Number"].ToString();
                                }
                                if (item["Phone"] != null)
                                {
                                    li.Phone = item["Phone"].ToString();
                                }
                                if (item["State"] != null)
                                {
                                    li.State = item["State"].ToString();
                                }
                                if (item["County"] != null)
                                {
                                    li.County = item["County"].ToString();
                                }
                                if (item["Zip"] != null)
                                {
                                    li.Zip = item["Zip"].ToString();
                                }
                                if (item["Address"] != null)
                                {
                                    li.PhysicalAddress = item["Address"].ToString();
                                }
                                p.LocationInformation = li;
                                if (item["Specialty"] != null)
                                {
                                    p.Specialty = item["Specialty"].ToString();
                                }
                                p.ProviderType = "Ancillary";
                            }
                            provList.Add(p);
                        }
                        catch (Exception e)
                        {
                            Logger.Error("Exception occurred " + e.Message);
                        }
                    }
                }
                return Ok(provList);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Exception occurred (Fatal) ", ex.Message);
                return BadRequest(ex.Message);
            }
        }
        [Route("GetPrimecompcompProviders")]
        [HttpGet]
        public IHttpActionResult GetPrimecompProviders()
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"GetPrimecompProviders{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
            try
            {
                List<Provider> provList = new List<Provider>();
                ClientContext clientContext = new ClientContext(url);
                clientContext.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");
                {
                    var web = clientContext.Web;

                    List list = web.Lists.GetByTitle("Physician Providers");
                    var listItemCollection = list.GetItems(CamlQuery.CreateAllItemsQuery());

                    clientContext.Load(listItemCollection,
                        eachItem => eachItem.Include(
                            item => item,
                            item => item["Status"], // status
                                                    //item => item["f4f2"], // NPI Number
                            Item => Item["ID"], //access ID which will replaced NPI
                            item => item["Contract_x003a_Fictitious_x0020_"], // Group  Name
                            item => item["Contract_x003a_Entity_x0020_Name"], // Entity Name
                                                                              //item => item["Facility_x003a_City"], // Facility City
                            item => item["Facility_x003a_City_x002d_2"], // Facility City
                            item => item["Facility_x003a_Facility_x0020_Fa0"], // Facility Fax
                            item => item["Facility_x003a_Facility_x0020_Ph0"], // Facility Phone
                            item => item["Facility_x002d_2_x0020__x0028_St"], // Facility
                            item => item["Facility_x003a_State_x002d_2"], // Facility State
                            item => item["Facility_x003a_Zip_x002d_2"], // Facility Zip
                            item => item["Secondary_x0020_Facility_x003a_C0"], // Secondary Facility City
                            item => item["Secondary_x0020_Facility_x003a_F1"], // Seconday Fax
                            item => item["Secondary_x0020_Facility_x003a_F2"], // Secondary Phone
                            item => item["Secondary_x0020_Facility_x002d_2"], // Secondary Facility
                            item => item["Secondary_x0020_Facility_x003a_S0"], // Secondary State
                            item => item["Secondary_x0020_Facility_x003a_Z0"], // Secondary Zip
                            item => item["Additional_x0020_Facility_x003a_"], // Additional Facility City
                            item => item["Additional_x0020_Facility_x002d_"], //Additional Facility
                            item => item["Additional_x0020_Facility_x003a_5"], // Additional Facility FAX
                            item => item["Additional_x0020_Facility_x003a_6"], // Additional Facility PHONE
                            item => item["Additional_x0020_Facility_x003a_7"], // Additional Facility State
                            item => item["Additional_x0020_Facility_x003a_8"], // Additional Facility Zip
                            item => item["Degree"],
                            item => item["First_x0020_Name"], // Provider First Name
                            item => item["Gender"],
                            item => item["Languages"],
                            // item => item["Title"], //? per Amelia, this isn't used and can be removed
                            item => item["Middle_x0020_Initial"], //Provider Middle Name
                            item => item["Title"], // last name
                            item => item["Specialty"],
                            item => item["Specialty_x0020_Notes"],
                            item => item["Practicing_x0020_Sub_x002d_Speci"], //Sub Specialty ??
                            Item => Item["Contract_x003a_site"])); // website




                    clientContext.ExecuteQuery();
                    foreach (var item in listItemCollection)
                    {
                        try
                        {
                            Provider p = new Provider();
                            if (item["Status"].Equals("Active"))
                            {
                                // scip if active id is missing
                                if (item["ID"] != null)
                                {
                                    p.ID = item["ID"].ToString(); // Access ID

                                    if (item["Contract_x003a_Fictitious_x0020_"] != null)
                                    {
                                        FieldLookupValue val = (FieldLookupValue)item["Contract_x003a_Fictitious_x0020_"];
                                        p.GroupName = val.LookupValue;
                                    }

                                    LocationInformation location = new LocationInformation();
                                    if (item["Facility_x003a_City_x002d_2"] != null)
                                    {
                                        //FieldLookupValue val = (FieldLookupValue)item["Facility_x003a_City_x002d_2"];
                                        location.City = item["Facility_x003a_City_x002d_2"].ToString(); //val.LookupValue;
                                    }

                                    //location.County = item[""]still waiting on this
                                    if (item["Facility_x003a_Facility_x0020_Fa0"] != null)
                                    {
                                        //FieldLookupValue val = (FieldLookupValue)item["Facility_x003a_Facility_x0020_Fa0"];
                                        location.Fax =
                                            item["Facility_x003a_Facility_x0020_Fa0"].ToString(); // val.LookupValue;
                                    }

                                    if (item["Facility_x003a_Facility_x0020_Ph0"] != null)
                                    {
                                        //FieldLookupValue val = (FieldLookupValue)item["Facility_x003a_Facility_x0020_Ph0"];
                                        location.Phone =
                                            item["Facility_x003a_Facility_x0020_Ph0"].ToString(); // val.LookupValue;
                                    }

                                    if (item["Facility_x002d_2_x0020__x0028_St"] != null)
                                    {
                                        //FieldLookupValue val = (FieldLookupValue)item["Facility_x002d_2_x0020__x0028_St"];
                                        location.PhysicalAddress =
                                            item["Facility_x002d_2_x0020__x0028_St"].ToString(); // val.LookupValue;
                                    }

                                    if (item["Facility_x003a_State_x002d_2"] != null)
                                    {
                                        //FieldLookupValue val = (FieldLookupValue)item["Facility_x003a_State_x002d_2"];
                                        location.State =
                                            item["Facility_x003a_State_x002d_2"].ToString(); // val.LookupValue;
                                    }

                                    if (item["Facility_x003a_Zip_x002d_2"] != null)
                                    {
                                        //FieldLookupValue val = (FieldLookupValue)item["Facility_x003a_Zip_x002d_2"];
                                        location.Zip = item["Facility_x003a_Zip_x002d_2"].ToString(); // val.LookupValue;
                                    }

                                    p.LocationInformation = new LocationInformation();
                                    p.LocationInformation = location;

                                    location = new LocationInformation();
                                    if (item["Secondary_x0020_Facility_x003a_C0"] != null)
                                    {
                                        //FieldLookupValue val = (FieldLookupValue)item["Secondary_x0020_Facility_x003a_C0"];
                                        location.City =
                                            item["Secondary_x0020_Facility_x003a_C0"].ToString(); // val.LookupValue;
                                    }

                                    //location.County = item[""]
                                    if (item["Secondary_x0020_Facility_x003a_F1"] != null)
                                    {
                                        //FieldLookupValue val = (FieldLookupValue)item["Secondary_x0020_Facility_x003a_F1"];
                                        location.Fax =
                                            item["Secondary_x0020_Facility_x003a_F1"].ToString(); // val.LookupValue;
                                    }

                                    if (item["Secondary_x0020_Facility_x003a_F2"] != null)
                                    {
                                        //FieldLookupValue val = (FieldLookupValue)item["Secondary_x0020_Facility_x003a_F2"];
                                        location.Phone =
                                            item["Secondary_x0020_Facility_x003a_F2"].ToString(); // val.LookupValue;
                                    }

                                    if (item["Secondary_x0020_Facility_x002d_2"] != null)
                                    {
                                        //FieldLookupValue val = (FieldLookupValue)item["Secondary_x0020_Facility_x002d_2"];
                                        location.PhysicalAddress =
                                            item["Secondary_x0020_Facility_x002d_2"].ToString(); // val.LookupValue;
                                    }

                                    if (item["Secondary_x0020_Facility_x003a_S0"] != null)
                                    {
                                        //FieldLookupValue val = (FieldLookupValue)item["Secondary_x0020_Facility_x003a_S0"];
                                        location.State =
                                            item["Secondary_x0020_Facility_x003a_S0"].ToString(); // val.LookupValue;
                                    }

                                    if (item["Secondary_x0020_Facility_x003a_Z0"] != null)
                                    {
                                        //FieldLookupValue val = (FieldLookupValue)item["Secondary_x0020_Facility_x003a_Z0"];
                                        location.Zip =
                                            item["Secondary_x0020_Facility_x003a_Z0"].ToString(); // val.LookupValue;
                                    }

                                    p.SecondaryFacilityAddress = new LocationInformation();
                                    p.SecondaryFacilityAddress = location;

                                    location = new LocationInformation();
                                    if (item["Additional_x0020_Facility_x003a_"] != null)
                                    {
                                        //FieldLookupValue val = (FieldLookupValue)item["Additional_x0020_Facility_x003a_"];
                                        location.City =
                                            item["Additional_x0020_Facility_x003a_"].ToString(); // val.LookupValue;
                                    }

                                    //location.County = item[""]
                                    if (item["Additional_x0020_Facility_x003a_5"] != null)
                                    {
                                        //FieldLookupValue val = (FieldLookupValue)item["Additional_x0020_Facility_x003a_5"];
                                        location.Fax =
                                            item["Additional_x0020_Facility_x003a_5"].ToString(); // val.LookupValue;
                                    }

                                    if (item["Additional_x0020_Facility_x003a_6"] != null)
                                    {
                                        // FieldLookupValue val = (FieldLookupValue)item["Additional_x0020_Facility_x003a_6"];
                                        location.Phone =
                                            item["Additional_x0020_Facility_x003a_6"].ToString(); // val.LookupValue;
                                    }

                                    if (item["Additional_x0020_Facility_x002d_"] != null)
                                    {
                                        //FieldLookupValue val = (FieldLookupValue)item["Additional_x0020_Facility_x002d_"];
                                        location.PhysicalAddress =
                                            item["Additional_x0020_Facility_x002d_"].ToString(); // val.LookupValue;
                                    }

                                    if (item["Additional_x0020_Facility_x003a_7"] != null)
                                    {
                                        //FieldLookupValue val = (FieldLookupValue)item["Additional_x0020_Facility_x003a_7"];
                                        location.State =
                                            item["Additional_x0020_Facility_x003a_7"].ToString(); // val.LookupValue;
                                    }

                                    if (item["Additional_x0020_Facility_x003a_8"] != null)
                                    {
                                        //FieldLookupValue val = (FieldLookupValue)item["Additional_x0020_Facility_x003a_8"];
                                        location.Zip =
                                            item["Additional_x0020_Facility_x003a_8"].ToString(); // val.LookupValue;
                                    }

                                    p.AdditionalFacilityAddress = new LocationInformation();
                                    p.AdditionalFacilityAddress = location;
                                    if (item["Contract_x003a_site"] != null)
                                    {
                                        FieldLookupValue val = (FieldLookupValue)item["Contract_x003a_site"];
                                        p.site = val.LookupValue;
                                    }

                                    p.Degree = (item["Degree"] != null) ? item["Degree"].ToString() : null;
                                    //p.Email = item["Email"] not on here
                                    p.FirstName = (item["First_x0020_Name"] != null)
                                        ? item["First_x0020_Name"].ToString()
                                        : null;
                                    p.Gender = (item["Gender"] != null) ? item["Gender"].ToString() : null;
                                    var languages = (string[])item["Languages"];
                                    if (languages != null)
                                    {
                                        int counter = 0;
                                        foreach (var language in languages)
                                        {
                                            if (counter == 0)
                                            {
                                                p.Language = language;
                                                counter++;
                                            }
                                            else
                                            {
                                                p.Language += "," + language;
                                            }
                                        }
                                    }

                                    p.LastName = (item["Title"] != null) ? item["Title"].ToString() : null;
                                    p.MiddleName = (item["Middle_x0020_Initial"] != null)
                                        ? item["Middle_x0020_Initial"].ToString()
                                        : null;
                                    p.ProviderType = "Physician";
                                    p.Specialty = (item["Specialty"] != null) ? item["Specialty"].ToString() : null;
                                    p.SpecialtyNotes = (item["Specialty_x0020_Notes"] != null)
                                        ? item["Specialty_x0020_Notes"].ToString()
                                        : null;
                                    p.SubSpecialty = (item["Practicing_x0020_Sub_x002d_Speci"] != null)
                                        ? item["Practicing_x0020_Sub_x002d_Speci"].ToString()
                                        : null;

                                    provList.Add(p);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Logger.Error("Exception occured " + e.Message);
                        }
                    }

                }
                return Ok(provList);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [Route("GetInfomatics")]
        [HttpGet]
        public IHttpActionResult GetInfomatics()
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"GetInfomatics{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
            try
            {
                using (var site = new ClientContext(Url))
                {
                    site.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");
                    var web = site.Web;
                    site.Load(web);
                    site.ExecuteQuery();

                    var list = web.Lists.GetByTitle("Documents");
                    site.Load(list);
                    site.ExecuteQuery();
                    site.Load(list.RootFolder);
                    site.ExecuteQuery();
                    site.Load(list.RootFolder.Folders);
                    site.ExecuteQuery();

                    try
                    {
                        foreach (var folder in list.RootFolder.Folders)
                        {
                            try
                            {
                                Logger.Info("Folder " + folder.ServerRelativeUrl);
                                if (!folder.ServerRelativeUrl.Contains("Forms"))
                                {
                                    processFolderClientObj(site, folder.ServerRelativeUrl, Url);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error("Exception occurred calling ProcessFolderClientObj " + ex.Message);
                                return BadRequest(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("Exception occurred " + ex.Message);
                        return BadRequest(ex.Message);
                    }
                }

                Logger.Info("Sharpeoint URL: " + Url);
                // input YOUR URL below
               // var site = new ClientContext(Url);
                

            }
            catch (Exception ex)
            {
                Logger.Fatal("Exception occurred (Fatal) " + ex.Message);
                return BadRequest(ex.Message);
            }

            return Ok();
        }
        private static void processFolderClientObj(ClientContext site, string folderUrl, string url)
        {
            try
            {
                // input YOUR URL below
               // var site = new Microsoft.SharePoint.Client.ClientContext(url);
                var web = site.Web;
                site.Load(web);
                site.ExecuteQuery();
                var folder = web.GetFolderByServerRelativeUrl(folderUrl);
                site.Load(folder);
                site.ExecuteQuery();
                site.Load(folder.Files);
                site.Load(folder.Folders);
                site.ExecuteQuery();
                const int logon32ProviderDefault = 0;
                //This parameter causes LogonUser to create a primary token.
                const int logon32LogonInteractive = 2;
                var returnValue = LogonUser(UserName, Domain, Pass, logon32LogonInteractive,
                    logon32ProviderDefault, out var safeTokenHandle);
                var text = new StringBuilder();
                if (false == returnValue)
                {
                    var ret = Marshal.GetLastWin32Error();
                    throw new System.ComponentModel.Win32Exception(ret);
                }
                using (safeTokenHandle)
                {
                    using (var newId = new WindowsIdentity(safeTokenHandle.DangerousGetHandle()))
                    {
                        using(var impersonateUser = newId.Impersonate())
                        {
                            foreach (var file in folder.Files)
                            {
                                try
                                {
                                    var destinationFolder = InformaticsOut + "/" + folder.ServerRelativeUrl;
                                    Logger.Info("Destination Locaiton: " + destinationFolder);
                                    var fs = Microsoft.SharePoint.Client.File.OpenBinaryDirect(site, file.ServerRelativeUrl).Stream;
                                    var binary = ReadFully(fs);
                                    
                                    if (!Directory.Exists(destinationFolder))
                                    {
                                        Directory.CreateDirectory(destinationFolder);
                                    }
                                    var stream = new FileStream(destinationFolder + "/" + file.Name, FileMode.Create);
                                    var writer = new BinaryWriter(stream, Encoding.UTF8);
                                    writer.Write(binary);
                                    writer.Close();
                                }
                                catch (Exception e)
                                {
                                    Logger.Error("Error retrieving file. Could be checked out: " + file.ServerRelativeUrl + " " + e.Message);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // need to put something here
                Logger.Error("Exception occurred saving files " + e.Message);
            }
        }
        private static byte[] ReadFully(Stream input)
        {
            try
            {

                var buffer = new byte[16 * 1024];
                using (var ms = new MemoryStream())
                {
                    int read;
                    while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, read);
                    }
                    return ms.ToArray();
                }
            }
            catch (Exception)
            {
                // need to put something here
                return null;
            }
        }
        private static IEnumerable<ListItem> GetPersons()
        {
            var ctx = new ClientContext(OnCall)
            {
                Credentials = new System.Net.NetworkCredential(_user, Password, "bh")
            };
            var list = ctx.Web.Lists.GetByTitle("Persons");
            var view = list.Views.GetByTitle("All Items");

            ctx.Load(view);

            var query = new CamlQuery();
            if (ctx.HasPendingRequest)
            {
                ctx.ExecuteQuery();
            }
            query.ViewXml = $"<View><Query>{view.ViewQuery}{query}</Query></View>";

            var items = list.GetItems(query);

            ctx.Load(items);

            if (ctx.HasPendingRequest)
            {
                ctx.ExecuteQuery();
            }

            return items.ToList();
        }
        private static IEnumerable<ListItem> GetSchedule()
        {
            var ctx = new ClientContext(OnCall) { Credentials = new NetworkCredential(_user, Password, "bh") };
            var list = ctx.Web.Lists.GetByTitle("Team Schedule");
            var view = list.Views.GetByTitle("All Items");

            ctx.Load(view);

            var query = new CamlQuery();
            if (ctx.HasPendingRequest)
            {
                ctx.ExecuteQuery();
            }

            query.ViewXml = $"<View><Query>{view.ViewQuery}{query}</Query></View>";
            var items = list.GetItems(query);

            ctx.Load(items);

            if (ctx.HasPendingRequest)
            {
                ctx.ExecuteQuery();
            }

            return items.ToList();
        }
        private sealed class SafeTokenHandle : SafeHandleZeroOrMinusOneIsInvalid
        {
            private SafeTokenHandle()
                : base(true)
            {
            }

            [DllImport("kernel32.dll")]
            [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
            [SuppressUnmanagedCodeSecurity]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool CloseHandle(IntPtr handle);

            protected override bool ReleaseHandle()
            {
                return CloseHandle(handle);
            }
        }
    }
}
