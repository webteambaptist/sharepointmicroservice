﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SharepointMicroservice.Models
{
    public class AttachmentsItem
    {
        string EntitySet { get; set; }
        int ItemId { get; set; }
        string Name { get; set; }
    }
}