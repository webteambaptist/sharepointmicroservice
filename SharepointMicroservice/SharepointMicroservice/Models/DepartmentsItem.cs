﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SharepointMicroservice.Models
{
    public class DepartmentsItem
    {
        public bool Attachments { get; set; }
        public string ContentType { get; set; }
        public string ContentTypeID { get; set; }
        public DateTime? Created { get; set; }
        public string CreatedBy { get; set; }
        public int? CreatedById { get; set; }
        public string Department { get; set; }
        public int Id { get; set; }
        public DateTime? Modified { get; set; }
        public string ModifiedBy { get; set; }
        public int? ModifiedById { get; set; }
        public int? Owshiddenversion { get; set; }
        public string Path { get; set; }
        public string Version { get; set; }
    }
}