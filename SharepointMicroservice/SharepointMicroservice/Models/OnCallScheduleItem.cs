﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SharepointMicroservice.Models
{
    public class OnCallScheduleItem
    {
        public bool Attachments { get; set; }
        public string ContentType { get; set; }
        public string ContentTypeID { get; set; }
        public DateTime? Created { get; set; }
        public string CreatedBy { get; set; }
        public int? CreatedById { get; set; }
        public string Department { get; set; }
        public string E1Phone1 { get; set; }
        public string E1Phone1_Type { get; set; }
        public string E1Phone2 { get; set; }
        public string E1Phone2_Type { get; set; }
        public string E1Phone3 { get; set; }
        public string E1Phone3_Type { get; set; }
        public string E2Phone1 { get; set; }
        public string E2Phone1_Type { get; set; }
        public string E2Phone2 { get; set; }
        public string E2Phone2_Type { get; set; }
        public string E2Phone3 { get; set; }
        public string E2Phone3_Type { get; set; }
        public DateTime? EndDateTime { get; set; }
        public string Escalation1 { get; set; }
        public string Escalation2 { get; set; }
        public int Id { get; set; }
        public DateTime? Modified { get; set; }
        public string ModifiedBy { get; set; }
        public int? ModifiedById { get; set; }
        public int? Owshiddenversion { get; set; }
        public string Path { get; set; }
        public string PPhone1 { get; set; }
        public string PPhone1_Type { get; set; }
        public string PPhone2 { get; set; }
        public string PPhone2_Type { get; set; }
        public string PPhone3 { get; set; }
        public string PPhone3_Type { get; set; }
        public string Primary { get; set; }
        public DateTime? StartDateTime { get; set; }
        public string Title { get; set; }
        public string Version { get; set; }
        public double? Wt { get; set; }
    }
}