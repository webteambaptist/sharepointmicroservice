﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SharepointMicroservice.Models
{
    public class Provider
    {
        public string ID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Specialty { get; set; }
        public string SpecialtyNotes { get; set; }
        public string SubSpecialty { get; set; }
        public string Gender { get; set; }
        public string Language { get; set; }
        public string Email { get; set; }
        public string ProviderType { get; set; }
        public string Degree { get; set; }
        public string GroupName { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }
        public string site { get; set; }
        public LocationInformation LocationInformation { get; set; }
        public LocationInformation AdditionalFacilityAddress { get; set; }
        public LocationInformation SecondaryFacilityAddress { get; set; }
    }
    public class LocationInformation
    {
        public string County { get; set; }
        public string PhysicalAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
    }
}