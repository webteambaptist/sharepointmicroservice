﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SharepointMicroservice.Models
{
    public class ScheduledQuarterlyDownTimePlansItem
    {
        public DateTime? ActualEndDate { get; set; }
        public DateTime? ActualStartDate { get; set; }
       // public List<UserInformationListItem> AssignedTo { get; set; }
        public List<string> AssignedTo { get; set; }
        public List<AttachmentsItem> Attachments { get; set; }
        public bool? Cancelled { get; set; }
        public string ChangeControlDescription { get; set; }
        public string ChangeControlNumber { get; set; }
        public DateTime? ChangeEndDate { get; set; }
        public DateTime? ChangeStartDate { get; set; }
        public Double? Complete { get; set; }
        public bool? Completed { get; set; }
        public string ContentType { get; set; }
        public string ContentTypeID { get; set; }
        public DateTime? Created { get; set; }
        public UserInformationListItem CreatedBy { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? DateOfScheduledDownTime { get; set; }
        public string DownTimeCoordinatorComments { get; set; }
        public int Id { get; set; }
        public DateTime? Modified { get; set; }
        public UserInformationListItem ModifiedBy { get; set; }
        public int? ModifiedById { get; set; }
        public int? Owshiddenversion { get; set; }
        public string Path { get; set; }
        public List<ScheduledQuarterlyDownTimePlansItem> Predecessors { get; set; }
       // public ScheduledQuarterlyDownTimePlansPriorityValue Priority { get; set; }
        public string PriorityValue { get; set; }
        public string TaskName { get; set; }
       // public ScheduledQuarterlyDownTimePlansTaskStatusValue TaskStatus { get; set; }
        public string TaskStatusValue { get; set; }
        public string Version { get; set; }
        public string Title { get; set; }

        public string Duration { get; set; }

        public string AssignedToName { get; set; }


        public string AssignedToPhone { get; set; }

        public string Description { get; set; }
    }
}