﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SharepointMicroservice.Models
{
    public class Vaccine
    {
        public string EEorNPI { get; set; }
        public string FirstName {get;set;}
        public string LastName {get;set;}
        public string Name {get;set;}
        public DateTime ApptTime {get;set;}
        public string ApptTimeString {get;set;}
        public string ConsentReceived { get; set; }
        public string BaptistEE { get; set; }
        public string DeptName { get; set; }
        public string JobTitle {get;set;}
        public string Dose {get; set; }
        public string Arm { get; set; }
        public string Reaction {get;set;}
        public string Notes {get;set;}
        public string CommunityOrAffiliate { get; set; }
        public string Location { get; set; }
    }
}